import java.util.List;

/**
 * Created by rvanbelk on 9/18/15.
 */
public class JVM21 {

    public static void main(String[] args) {

        Long calcNum; //calculated number in chain
        int numCounter = 1; //count of numbers in chain
        int highestChain = 0; //longest chain
        int highChainStartingNumber = 0; //the starting number to chain with highest chain length

        for (long i = 1L; i <= 1000000; i++) {
            calcNum = i;
            while (calcNum != 1) {
                if ((calcNum % 2) == 0) { //number is even
                    calcNum = calcEven(calcNum);
                } else { //number is odd
                    calcNum = calcOdd(calcNum);
                }
                numCounter++;
            }//end while

            if (highestChain < numCounter){
                highChainStartingNumber = (int)i;
                highestChain = numCounter;
            }//end if

            numCounter = 0;
        }//end for

        System.out.println("\nLONGEST_CHAIN = " + highestChain);
        System.out.println("LONGEST_CHAIN_STARTING_NUMBER = " + highChainStartingNumber);
    }

    private static Long calcOdd(Long x) {
        Long num = 3 * x + 1;
        return num;
    }

    private static Long calcEven(Long x) {
        Long num = x / 2;
        return num;
    }


}



